#!/usr/bin/env python

import sys, os
import glob
import json
import subprocess
import collections
import re
import stat

'''This will run tests on analysis templates located in templates
1) get all analysis templates
2) get test set metag
3) run analysis on local, denovo-debug, cori-debug
4) Compare results, unit test

'''
DEBUG=False
def main():
    '''main'''
    #cleanup tmp 
    tmp_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","tmp")
    if not(os.path.exists(tmp_dir)):
        sys.exit(tmp_dir + " Does not exist")
    cmd = "rm -rf " + tmp_dir + "/*"
    if not(DEBUG):
        out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
    runner=glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","bin","pipeline_runner.py"))[0]
    test_sets = glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","test_data","*.gz"))[0]
    test_bash_files = list()
    analysis_templates = glob.glob(os.path.join(os.path.dirname(os.path.realpath(__file__)),"..","templates","analysis.*"))
    for template in analysis_templates:
        output_dir = os.path.join(tmp_dir,os.path.basename(template) + ".dir")
        template_out = os.path.join(output_dir,"run.yaml")
        run_bash = os.path.join(output_dir,"run.yaml.bash")
        #run_cori = os.path.join(output_dir,"run.yaml.bash.cori.slurm")
        #run_denovo = os.path.join(output_dir,"run.yaml.bash.cori.slurm")
        if not(DEBUG):
            os.mkdir(output_dir)
        with open(template,"r") as f: template_s = re.sub(r'__INPUT_FILES__', test_sets, f.read())
        with open(template_out,"w") as f:f.write(template_s)
        cmd = sys.executable + " " + runner + " -f " + template_out
        create_bash_debug_job(cmd, run_bash,job_type='local')
        test_bash_files.append(run_bash)
    
    for bash in test_bash_files:
        outfile = bash + ".out"
        cmd = bash + " > " + outfile
        if not(not(DEBUG)):
            print("Running " + bash)
            out = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE).stdout.readlines()
            with open(outfile, "r") as f:lines = f.readlines()
            if lines[-1].find("Complete") != -1:
                print(bash + " pass")
        else:
            print(bash)
    sys.exit()

def create_bash_debug_job(cmd,filepath,job_type='local'):
    bash_file = '#!/usr/bin/env' + " bash\n\nulimit -c 0\numask 002\nset -eo pipefail\nunset PYTHONPATH\n"
    bash_file += bash_debug_job_modifiers(job_type) + "\n\n"
    bash_file += "cd " + os.path.dirname(filepath) + "\n\n"
    bash_file += cmd + "\n"
    with open(filepath, "w") as f: f.write(bash_file)
    os.chmod(filepath, 0o755)
    return filepath
    

def bash_debug_job_modifiers(job_type='local'):
    return_string = ""
    if job_type=='cori':
        return_string = ""
    elif job_type=='denovo':
        return_string = ""
    else:
        pass
    return return_string

def hasher():
     '''supports perl style auto-vivification of arbitry depth dicts'''
     return collections.defaultdict(hasher)
 
if __name__ == "__main__":
    main()

'''    parser = argparse.ArgumentParser(description='Description.')
    parser.add_argument("-f", "--file", required=True, help="comma separated files to assemble and map.\n")
    parser.add_argument("-t", "--template", required=False,  help="filtered file to assemble and map. default is " + DEFAULT_TEMPLATE )
    parser.add_argument("-r", "--run_dir", required=False, help="run_dir to run. default is current working directory\n")
    parser.add_argument("-o", "--outputfile", required=False, default="run.yaml", help="runfile. default = run.yaml\n")
    parser.add_argument("-x", "--execute", required=False, default = False, action='store_true', help="execute using surm.\n")
    parser.add_argument("-l", "--local", required=False, default = False, action='store_true', help="execute locally.\n")
    parser.add_argument("-c", "--cluster_name", required=False, default ="metat", help="cluster name.\n")
    parser.add_argument("-d", "--debug", required=False, default = False, action='store_true', help="debugging.\n")
    parser.add_argument('-v', '--version', action='version', version="%(prog)s (version " + VERSION + ")")

    args = parser.parse_args()

    if not os.path.exists(args.file):
        #=======================================================================
        # sys.exit("Can't find " + args.file)
# #         #=======================================================================
    inputs = " ".join(args.file.split(","))
        
    
    if not args.template:
        args.template = os.path.dirname(os.path.realpath(__file__)) + "/" + DEFAULT_TEMPLATE
    with open(args.template) as f:
        template = f.read()
    template = re.sub(r'__INPUT_FILES__',inputs,template)
    
    if not args.run_dir:
        args.run_dir =  os.getcwd()

    runfile = args.run_dir.rstrip("/") + "/" + os.path.basename(args.outputfile)
    with open(runfile, "w") as f:
        f.write(template)
'''
